---
title: "About"
date: 2021-06-03T20:14:08-04:00
draft: false
---

# About Me

Curious of all things unknown, I've always found gratification in finding things that subvert my expectations.  Like most people, I want to understand and contribute.

In 2017 I graduated from Plattsburgh State with a Bachelors in Computer Science with a focus on Software Development and have been neck deep in code ever since.

Most days you can find me behind my desk programming.  My love of Data Science is both humbling and fascinating.  In the pursuit of answers I always find more questions, and in these questions, I find purpose.   When I'm not behind my desk I'm outside Disc Golfing or with my dog, Freya.

![Bubby](/dylanjackson/images/bubby.jpg)

