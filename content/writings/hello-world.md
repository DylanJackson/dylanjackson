---
title: "Hello World"
date: 2021-06-04T16:56:25-04:00
draft: false
---

# Hello, World!

Thank you for checking out my blog and thank you for traveling this far into my posts history.  I wasn't always keen on creating a blog.  Anyone who has considered a career in programming or anything digital has no doubt heard that they needed an online presence to show to employers.  For me though that didn't really sit right.

Creating a blog for the purpose of putting myself on display seemed artificial and just unpleasant.  The value it created was self-serving and superficial.  My opinion shifted when I began to look at this as more of a git repository for myself.  Any can go to their favorite open source project's git repository and look at its humble beginnings.  We can look through the commits and see what has changed and how it grew.  Anyone looking to do something similar can gain insight from that, and I believe there's insight in this too.

This is my humble beginning, my first commit so to speak.  There is so much I want to learn, and so much I want to create.  I look forward to cataloging my progress as a student, programmer, aspiring data scientist, and individual. 

For anyone else at their humble beginnings, I hope this blog brings you value.
