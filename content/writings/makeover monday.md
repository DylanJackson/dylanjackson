---
title: "Makeover_Monday"
date: 2021-06-09T09:52:46-04:00
draft: false
---

# Makeover Monday

Currently, I'm working my way through a Tableau course, and I'm in awe with this tool.  Tableau, for those who are unfamiliar, is data visualization software.  I still have quite a ways to go before I'll feel truly comfortable navigating sub-menus and finding where everything is but, with what I do know now, I'm already creating concise and nice looking visualizations.

As with anything, repetition is key, and finding projects to work on outside of classes and courses is essential to developing further.  Queue #MakeoverMonday.

Every week, Makeover Monday releases a new dataset to be modeled and visualized.  These datasets are vast and cover topics from Malaria in Zimbabwe to the warming of the Arctic Circle.  The community dissects the data and published their findings on Twitter using #MakeoverMonday.

In terms of modeling and visualization, this seems like a great opportunity to work on some unique data sets and participate in the Data Science community.  I'm sure I'll be updating this blog with my thoughts and progress to stay tuned for more.

