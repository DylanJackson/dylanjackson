---
title: "Disc_golf"
date: 2021-06-26T11:56:04-04:00
draft: false
---

# Disc Golf and Data Analysis

https://public.tableau.com/app/profile/dylan.jl/viz/DiscStats0_1/DiscGolfDashboard

I remember my first year of disc golf, 30 foot throws directly into the woods, more time spent searching for my discs than actually playing.  It's been 4 years since I started and let me tell you I do not miss those days.  I still occasionally play the disc-shaped Easter egg hunt in the woods, but much less frequently.  The progress of my friends and is apparent, yet abstract, until last month that is.  In the interest of tracking our improvement, I've begun logging our game stats and creating visualizations for us.  In the short period of time, I have been aggregating our data, clear trends and points of interest have already begun to emerge.

## Questions:

In the beginning, I looked to answer some simple fundamental questions.  Questions like; how have our scores changed through the season? and How do we perform on a different course?  To answer these questions getting basic information such as hole par, course total par, and our scores would be sufficient.  Data analysis is inherently exploratory as well so any points of interest would undoubtedly present more questions and opportunities to explore the data I had collected.

## Preparation and Processing:

I had decided on three data sets for processing:

1. Player performance on a course for a given day.
2. Course Stats such as hole par
3. Course latitude and longitude for visualization

#### Player performance on a course for a given day:



![individual_stats](/dylanjackson/images/individual_stats.PNG)

My first thought was to create a separate column for each hole, this would cut down on the amount of data I would have to enter substantially. I would only have to enter the course, date, time of day, and individual once for each game we play.  Another benefit would it would be more readable for us humans.  As we all learn, however, appeal and convenience in data entry do not mean the same with processing and analyzing.  Had I kept the original format I would have to form 18 separate relationships for each hole field.  With this solution, I only need to form one.

#### Course Stats

![course_stats](/dylanjackson/images/course_stats.PNG)

This data set is pretty rudimentary, each row contains the hole number, hole par, and course name.

#### Course Location:

![course_location](/dylanjackson/images/course_location.PNG)

Lastly, the location of each course would be used when visualizing the data.

## Analysis and Visualization:

My tool of choice for this is Tableau, I have a link to the latest version of this dashboard at the top of the page.

I wanted a way to be able to view stats from varying granularity.  The viewer should ideally be able to see everything from stats from an individual game to their overall performance for a given course.  For this, I settled on what I call Average Par Difference (APD).  APD is the sum of the players' scores for a given hole, divided by the holes par multiplied by the number of games played.  What this means is that with an APD of 0 the player is on average making par for that given hole.  At the highest granularity, we can see how the player performed on average for each course from beginning to end date.

![APD_high_gran](/dylanjackson/images/APD_high_gran.PNG)

Another Point of interest is we are able to see anomalies in the players' performance.  In this case, we immediately see that hole 11 on the Cadyville course causes trouble for this particular player.  We can also see that they should not be too concerned with hole 17 at Ausable Chasm.  This view allows the players to quickly see which holes they need to practice improve their score.

With this same method, we are also able to view precisely how the player played on a singular date.  At the lowest granularity, course, and date, the data represents not an average but the players' raw stats.

![APD_low_gran](/dylanjackson/images/APD_low_gran.PNG)

At the highest granularity, we can also calculate the best theoretical performance a player can have on a course by taking their best scores for each hole.  This allows the player to see the difference between how well they played on a certain day compared to how well they could have played.

![best_plays](/dylanjackson/images/best_plays.PNG)

#### Upcoming Analysis

There is data I have yet to incorporate into the analysis including the time of day and weather.  It would be interesting to see if certain players performed better at certain times, as well as if the weather had any effect on performance.  Personal experience tells me there would definitely be a disparity between games played on sunny days vs rainy days.  There is also data I have already incorporated but could be explored further.  Comparing the players' performance against each other as well as visualizing our change in score for a given course over a period of time.  Again, personal experience tells me Cody will annihilate us all in this but I'm curious nonetheless.

#### Conclusion

Although this is still early on there are already several pieces of analysis that I find useful.  In particular, knowing trouble holes gives players an area of focus for their improvement.  Also knowing how well we could theoretically play at our best gives us a hopeful goal to strive for as well as a reminder on our bad days that we are fully capable of playing well.  I can't wait to dig into this further and turn this project into something that can be used by myself and my friends to improve.
