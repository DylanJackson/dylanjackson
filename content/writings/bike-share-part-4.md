---
title: "Bike Share Part 4"
date: 2021-07-23T16:05:47-04:00
draft: false
---

# Bike-Share Part 4: Baseline Analysis and Visualization

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

This is the part where we really get to see the fruits of our labor.  We can see this through out the process, but, we finally get to plot our data, find insights, and format it into a user-friendly manner.  Gone are the days of manually renaming cells in excel!  Let's get to it.

Today will be focused primarily on some baseline visualizations.  A brief visualization of the landscape gives the viewer an opportunity to gain their bearings before digging into the finer point.

![baseline](/dylanjackson/images/bike-share/baseline.PNG)

Even in this brief overview, we can already pick apart some trends.  Casual usage seems to spike on the weekends and during the warmer months.  From an advertiser's standpoint, it's important to know the times where your message can reach the most eyes.  

We also see that casual users make up over 50% of rides in 7 out of the 10 most popular stations.  This is another useful statistic as these locations can be used as reference points for ad locations, or potentially, as places to perform surveys among the users to get more detailed information on casual users.

Another point of interest is the overall ratio of rides between members and casual users.  Casual users account for only 42% of the total rides.  I find it interesting that even though casual users make up a noticeably smaller percent of the total rides, they still contribute significantly to noticeable trends and even have their own unique caveats.

 The last visualization we'll look at is the average ride duration over time.  I find this visualization in particular interesting as the outcome is counterintuitive.  The company this information was derived from has additional charges for rides above 30 minutes if you are not a member.  With that in mind, the fact that casual riders take longer trips consistently throughout the year is a major point of interest that should be explored further.

In the next post, I'll be breaking down ride duration more.  We'll look at how the users differ and what the company can do to capitalize on this discrepancy.
