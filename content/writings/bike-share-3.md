---
title: "Bike Share Part 3"
date: 2021-07-18T14:09:58-04:00
draft: false
---

# Bike-Share Case Study Part 3: Datetimes and Weekdays

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

Today is a quick one as we'll be reformatting the start and end time columns as POSIXct data types and extracting some simple info from that.  We'll be using the Lubridate library as it makes working with dates effortless.

```R
tripData$started_at <- as_datetime(tripData$started_at, tz="America/Chicago")
tripData$ended_at <- as_datetime(tripData$ended_at, tz="America/Chicago")
```

With the proper data type, we can now do some simple arithmetic on it.  Let's get rid of rows that have a started_at time greater than its ended_at time.

```R
tripData <- tripData %>% 
  filter(started_at < ended_at)
```

Now, we can calculate the duration of each trip in minutes.

```R
tripData$trip_duration <- 
  as.double(difftime(tripData$ended_at,
                   tripData$started_at,
                   units = "mins"))
```

And lastly, let's get each ride was on.

```R
tripData$weekday <- weekdays(tripData$started_at)
```



That's about it for our data cleaning.  Just to ensure I had a complete set of data the drop_na function from the Tidyr package and then compared the results to the original data frame

```R
nrow(tripData) - nrow(drop_na(tripData))
```



The result was 0, meaning no rows were dropped and no nulls were found.  This may not be the most efficient method to check for nulls but it's a quick and simple way to set up the test.  Now it's time to move on to analysis.
