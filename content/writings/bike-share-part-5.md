---
title: "Bike Share Part 5"
date: 2021-07-30T13:14:31-04:00
draft: false
---

# Bike-Share Part 5: Trip Duration

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

Last post I gave a general look at the landscape of our data.  I presented some baseline visualizations that gave a brief look into what will come next.  Today is all about trip duration.

As I mentioned in the prior post I had done some independent research and found that this information had been derived from a company by the name of Divvy.  In that research, I learned that Divvy has a policy for Casual rides concerning trip duration.  Specifically, a Casual rider will accrue additional costs for rides that go over the 30-minute mark.  Once this kicks in, riders will be charged additionally by the minute.

Before I started exploring this data further I wanted to make sure that it wasn't being skewed by any outliers as I would be using averages.  I found that there were instances of rides days.  One ride, in particular, was over 40 days long.  Though these instances were few and far between I decided to exclude rides that were over a day long.  This seemed like a reasonable time frame to include only riders who were conscientious of their ride duration.

With this in mind, I was surprised to find out that by every metric, Casual riders take longer rides on average than Members.  Let's take a look.

![Trip Duration](/dylanjackson/images/bike-share/trip_duration.PNG)

if we look at the Pie Charts we see that 35% of all rides taken by Casual Users were over 30 minutes.  Contrast that with Members, in which only 11% of their rides are over 30 minutes, and it's clear that proportional to their group, Casual riders take more rides of a longer duration.  In the last Pie Chart explore the proportion of rides over 30 minutes between Casual users and Members.  A staggering 70% of all rides over 30 minutes are done by Casual users.

So we now know that Casual users take longer rides more frequently and in greater numbers than members.  If we look at our bar chart we see that this also holds by weekday, and if we look at our ride duration over time, this also applies throughout the entire year without exception.

For our last chart, Ratio of Rides by Trip Durations, I wanted to take a look at the proportions between groups for different durations.  For the purpose of clarity and readability, I did not include times past 500 minutes, but the trend remained the same up to the cut-off point of 1440 minutes.  I find these results to be completely counterintuitive.  What I expected was that Members would be responsible for a greater percentage of rides as the rides got longer as Members would not accrue additional costs.  Instead, we see the opposite.  If you only saw this chart you would infer that casual users have an incentive to ride for longer durations.  Interestingly enough, we see that right at the 30-minute mark is when casual riders become responsible for a greater proportion of rides, as that is the exact moment additional costs start to accrue.

**So what can we gather from this?  How can we use this information to plan and act accordingly?**

I think the answer to this lies in two things, education, and reward.

An increased emphasis on the additional costs of longer rides could influence a rider to opt for a membership.  We could also make certain statistics available to the user through Divvy's app.  Statistics such as the total additional costs they have accrued and the total amount of time spent on rides could arm the user with the information they realize the cost-benefit of a membership.

Another potential route would reward these users through their investment in time and money.  A discount could be applied to the cost of membership based on a user's frequency of use, total mileage, or total time on rides.  As the user utilizes the service the cost would come down and make a membership purchase more justifiable.

Rewarding a user for their continued patronage would not only increase membership but user perception of the company.  Casual users know that the company wants their membership, this is apparent with the added benefits of being a member.  An accumulative discount on membership based on the user's statistics means the customer's time and money go not only to the company but themselves.  It's a way for the company to say "we see you and you matter as well."  I firmly believe this would increase memberships.



Overall I believe this information provides unique angles to maximize membership potential.  The cost of longer rides and the incentive for the user to avoid that can be utilized in multiple way that can both benefit the company and the user.  Next time, we'll look at rider usage by the weekday and over time.  This should provide some useful information for advertisers who want to maximize casual user exposure to advertisements.



## Just For Fun:

The longest ride by a casual user was 58720 minutes!  Let's see how much that costs.

- 30 Minute ride: **$3.50**
- Each minute over 30 Minutes: **$0.15**
- Cost of a ride = 3.50 + (0.15 * 58720) = **$8,811.50**

I hope the customer was able to resolve this because that ride was not cheap!
