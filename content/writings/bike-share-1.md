---
title: "Bike Share Part 1"
date: 2021-07-15T22:51:32-04:00
draft: false
---

# Bike-Share Case Study Part 1: Duplicate Rows

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

The first order of business should always be weeding out duplicate entries.  Duplicates skew data and make it appear as though there is significance where none lies.  Conveniently, our bike-share contains a unique value for each row of data.  In DB Browser we can quickly group by ride_id and show only those which have more than one entry.  This is what we get.

```SQL
SELECT ride_id, COUNT(*) as cnt
	FROM tripdata
	GROUP BY ride_id
	HAVING cnt > 1
```



![duplicate_ids](/dylanjackson/images/bike-share/duplicate_ids.PNG)

209 duplicate entries, with each duplicate ride_id appearing twice.  We could stop there and use this information to delete one of every duplicate row.  However, it's probably worth exploring further.  The systems which generated this information are automated.  As such, we may find that other fields have incorrect information as well.  It could be that none of these variables can be or used, or only one of them has the correct information.

Either way, let's modify this query so we have the rest of the information these duplicates.

```sql
SELECT *
FROM tripdata
WHERE ride_id IN 
	(SELECT ride_id
	FROM tripdata
	GROUP BY ride_id
	HAVING COUNT(*) > 1)
ORDER BY ride_id
```

![duplicate_variables](/dylanjackson/images/bike-share/duplicate_variables.PNG)

All else being the same, it appears as though the duplicates have incorrect start times.  Something went wrong for a brief period of time and as a result, 2 entries for each ride were generated, with one entry having their started_at month be 1 month later.  At a closer look it also appears as those this bug occurred on a single day as the duplicates all have a starting date of 2020-11-15 or 2020-12-15

This is convenient for us as we can now simply grab those entries which have a month of 12.  When we modify our query to accommodate we get this.

```SQL
SELECT *
FROM tripdata
WHERE ride_id IN 
	(SELECT ride_id
	FROM tripdata
	GROUP BY ride_id
	HAVING COUNT(*) > 1)
	AND strftime('%m', started_at) = '12'
ORDER BY ride_id
```

We can then export these values and run them against our combined data in R Studio.

```R
tripData <- read.csv(file = "tripdata.csv",fill = TRUE,na.strings=c("","NA"))
duplicate_values <- read.csv(file = "duplicate_values.csv", fill = TRUE, na.strings=c("","NA"))

tripData <- filter(tripData,
  !(tripData$ride_id %in% duplicate_values$ride_id &
  tripData$started_at %in% duplicate_values$started_at))
```

This is done in 5 parts. 

- First, we find all rows which have the same ride_ids as those which are incorrect duplicates.  
- Then we find all rows which have the same started_at times as the duplicated rows.  

The results we get are two binary vectors whose TRUE values are at the same indices as where the comparisons were true.  

- We then run the 'and' (&) logical operator on these two tables leaving us with a binary vector with TRUE values where our incorrect duplicates are located.
- To complete our mask, we invert our mask with the not (!) logical operator giving us TRUE values in the vector wherever we do not have an incorrect duplicate.

- We apply this mask to the tripData table using the filter function and we now have a table without those duplicate values.

Next, we will focus on cleaning up station names and determining a method to get a set of reliable and accurate coordinates for them.

## Quick Look at the Bug

Just for fun, let's take another look at that bug.  With the data given we can get a rough estimate of how long this bug lasted

```SQL
SELECT max(started_at) as max_time, min(started_at) as min_time, 
	strftime('%s',max(started_at)) - strftime('%s',min(started_at)) as duration_of_bug
FROM tripdata
WHERE ride_id IN 
	(SELECT ride_id
	FROM tripdata
	GROUP BY ride_id
	HAVING COUNT(*) > 1)
	AND strftime('%m', started_at) = '12'
```

![bug_duration](/dylanjackson/images/bike-share/bug.PNG)

That's **47 Minutes and 29 Seconds** of ride entries being duplicated.
