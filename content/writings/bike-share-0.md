---
title: "Bike Share Part 0"
date: 2021-07-15T16:48:36-04:00
draft: false
---

# Bike-Share Case Study Part 0: The Introduction

In June I began the [Google Data Analytics](https://www.coursera.org/professional-certificates/google-data-analytics?) course on Coursera.  For our final project, we are tasked with completing a case study for a fictional bike-sharing company.  The task is to determine how annual members and casual riders use their service differently.  The goal of understanding their differences is to design marketing strategies aimed at converting casual riders into annual members.  To do this I have been supplied the previous 14 months of trip data.

## The Tools

| Purpose                        | Tool                |
| ------------------------------ | ------------------- |
| Data Viewing and Cleaning      | LibreOffice Calc    |
| SQL Data Modeling and Cleaning | SQLite / DB Browser |
| Data Modeling and Analysis     | RStudio             |
| Data Visualization             | RStudio / Tableau   |

## The Data

4,358,611 rows of data spread across 14 excel sheets, separated by month.  Lets take a look.

![clean_data](/dylanjackson/images/bike-share/clean_data.PNG)

![dirty_data](/dylanjackson/images/bike-share/dirty_data.PNG)

At a glance, the information is mostly complete.  But in the later months, there are notable gaps.  Luckily, the headers are consistent, so I can quickly toss this together and check for nulls with SQL.

```SQL
SELECT COUNT(*) - COUNT(ride_id) AS ride_id_nulls,
	COUNT(*) - COUNT(rideable_type) AS rideable_type_nulls,
	COUNT(*) - COUNT(started_at) AS start_at_nulls,
	COUNT(*) - COUNT(ended_at) AS ended_at_nulls,
	COUNT(*) - COUNT(start_station_name) AS start_name_nulls,
	...
FROM tripdata
```

![null_count](/dylanjackson/images/bike-share/null_count.PNG)

That’s a lot of nulls. Thankfully, it seems the nulls are constrained to only a few columns. There are a few other inconsistencies such as inconsistent station names, id’s and coordinates, but we'll look into that later.

The first logical step is to locate any potential duplicate rows.
