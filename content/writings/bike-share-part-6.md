---
title: "Bike Share Part 6"
date: 2021-08-05T12:05:08-04:00
draft: false
---

# Bike-Share Part 6: Ride Stats and Time Granularity

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

Today's post is going to be all about time.  We're going to be looking at the key differences between Casual users and Members at different time granularities.  First off, let's look at the weekdays.

![weekday](/dylanjackson/images/bike-share/weekday.PNG)

First, we will look at the Weekday vs Weekend Ratio pie charts.  These pie charts illustrate the ratio of rides which are taken on weekdays, with rides on the weekend.  With these simple pie charts, we can already see the theme of this page emerging.  Casual users are more frequent weekend riders.

Let's break this down a bit more with Casual vs Members: Percent of Riders by Weekday.  Members have a relatively even distribution between the weekdays, varying by at most 3%.  By contrast, we see Casual users have a clear preference for riding on the weekends, with a variance between Tuesday and Saturday of 14%.  There is a slight preference for Friday as well, but insubstantial compared to Saturday and Sunday.

If we take a look at the Ratio of Rider Type by Weekday and Weekend we can begin to compare directly between the two groups.  On Weekdays, Members account for 64% of Rides, yet, on the Weekend, only 48%.  Casual riders make up the majority of weekend rides despite making up only 42% of all rides taken.  For a group to be responsible for fewer rides overall, and still, make up the majority of rides on only two days shows a definite contrast between the two users.

Let's zoom this out and take a look at these stats on a month-to-month basis.

![seasonal](/dylanjackson/images/bike-share/seasonal.PNG)

Number of Rides by Month and Rider shows us each group and their contribution to the month's ride total.  An area chart seemed like a good idea for this insight as we could illustrate several pieces of information with it.  We can see the total number of rides for any given month, as well as by Rider Type, and visually see the proportions between the two groups.

Both groups appear to be seasonal.  We can see the numbers rise in the warmer months and fall in the colder, before beginning to rise again.  The magnitude of seasonality is quite different, however.  In Percent of Casual Riders over Time we look at the percent of rides Casual users are responsible for, for each month.  In the warmer months, we see Casual users come close to half of all rides taken.  By contrast, we see Casual usage falls as low as only 18% of all rides taken in January 2021.

While they may both be seasonal, proportionally, Casual users are substantially seasonal.  In summation, we could say Casual users prefer the warmer months and are more likely to ride on the weekend than their Member counterparts.

**So what can we gather from this?  How can we use this information to plan and act accordingly?**

I find this information particularly informative from an advertising aspect.  Advertising is not cheap, especially in a city like Chicago.  It's imperative that Return on Investment (ROI) be maximized, and to do that,  ads need to reach the target demographic at the optimal times.

To aid with this I created another dashboard.  Unlike the others, this one was not made to highlight differences between users, but rather, to help advertisers find the key times to show ads.  Let's take a look.

![optimal_times](/dylanjackson/images/bike-share/optimal_times.PNG)

This dashboard is different as it functions as a dynamic visual aide for finding the optimal times and locations to display ads.  For each granularity of time, the user is given the 5 stations most frequented by casual users.  Each granular of time can also be filtered.  By selecting a time, weekday, and/or month, the user can find the optimal stations for ads in that particular time frame.

For example, let's say we wanted to know the popular stations on a Saturday in October.

![saturday_october](/dylanjackson/images/bike-share/saturday_october.PNG)

From this, we can see that 5 pm to 10 pm is the perfect time to display ads around Lake Shore Dr & Monroe St.

![sunday_may](/dylanjackson/images/bike-share/sunday_may.PNG)

On a Sunday in May, we see that between 5 pm and 2 am, Streeter Dr & Grand Ave would be the best location to advertise around.

I find tools like this particularly interesting.  It's a great reminder that data analysis isn't just about providing insights, but about empowering others to do the same.  If I can make something that others can use and derive value from, then I know I've done my job.

That's it for visualizations.  In the next post, I'll be summarizing everything we've learned and give my final thoughts on the project as a whole.

