---
title: "Bike Share Part 2"
date: 2021-07-16T15:13:22-04:00
draft: false
---

# Bike-Share Case Study Part 2: Station Name and Coordinate Cleaning

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

If we want proper association between the data we're going to need clean station names.  Each entry contains the starting and ending station name, as well as the station's geographic coordinates.  The coordinates vary for individual stations so getting proper names will be essential in determining proper geographic coordinates.

## Proper Station Names

A quick glance in DB Browser shows we have 718 unique names not counting null.

```SQL
SELECT DISTINCT(station_name)
FROM
	(SELECT start_station_name AS station_name
	FROM tripdata
	UNION ALL
	SELECT end_station_name AS station_name
	FROM tripdata) A
```

Alternatively, you could run this in R Studio to get the same results.

Note: I use the Tidyverse package for pipes (%>%), as well as the select, rename, drop_na, and unique functions.

```R
tripData <- read_csv("trip_data.csv")

# Get starting_station_names from tripData and rename the column 'station_name'
start_names <- tripData %>% 
  select(start_station_name) %>% 
  rename(station_name = start_station_name)

# Get ending_station_names from tripData and rename the column 'station_name'
end_names <- tripData %>% 
  select(end_station_name) %>% 
  rename(station_name = end_station_name)

# Combine starting_station and ending_station names then drop NA and non-unique values.
station_names <-
  rbind(start_names,end_names) %>% 
  drop_na() %>%
  unique()
```

Once we have our unique station names we open it in Excel and do some manual cleaning.

After sorting the names and looking at the data we see several instances of duplicated station names.

![dirt_names](/dylanjackson/images/bike-share/dirty_names.PNG)

For the most part, these names are not bad.  Most inconsistencies are the result of the station being renamed to denote they are close to a vaccination center or contain another name in a set of parenthesis.  To aid in finding these we can use conditional formatting and an excel formula.

```
=ISNUMBER(SEARCH("(",A2))
=ISNUMBER(SEARCH("Vacc",A2))
```

In essence, SEARCH returns the position of a substring within the text.  We can use that to find parenthesis and Vacc(for vaccination) in our names.  Next, we wrap the SEARCH function in ISNUMBER.  This will result in us getting TRUE or FALSE depending on whether or not the name contains any of the substrings.  We can apply this to all columns to check each name, and then apply conditional formatting to highlight the TRUE values.  It looks something like this.

![station_name_fix](/dylanjackson/images/bike-share/station_name_fix.PNG)

Now it's just a matter of scrolling through the data manually.  A separate column will be made that will contain the proper name for each station name found in the data regardless of whether the name is incorrect or not.  The columns we added will aid us, but it's important to look at each value individually.

![name_association](/dylanjackson/images/bike-share/name_association.PNG)

This is great for us as now we can use VLOOKUP to change the names in the raw excel files.  We can also begin applying the proper names in order to aggregate data by their name.

## Geographic Coordinates

Now that we have the proper names let's combine that with our combined data in R Studio

```R
fixed_station_names <- read_csv("fixed_station_names.csv")

# Get starting station names and coordinates
start_data <- tripData %>% 
  select(start_station_name, start_lat, start_lng) %>% 
  rename(station_name = start_station_name, lng = start_lng, lat = start_lat)
# Get ending station names and coordinates
end_data <- tripData %>% 
  select(end_station_name, end_lat, end_lng) %>% 
  rename(station_name = end_station_name, lng = end_lng, lat = end_lat)
# Merge starting and ending data
station_data <- start_data %>% 
  rbind(end_data) %>% 
  drop_na()
# Inner join the fixed names with the name and coordinates table
station_data <- inner_join(station_data, fixed_station_names)
```

The resulting data frame, station_data, contains the raw station name, the proper station name, latitude, and longitude, for every row of data we have.

Now we need a reliable way to get a uniform latitude and longitude for each station.  If we group each entry by the proper name we assigned them we can perform an aggregate function to get latitude and longitude.  

### Average or Median

My first thought was to calculate the average.  On a surface level getting the calculated middle of all data makes sense.  However, I later decided that median would be best.  My reason for this was that the average factors in all data.  This means a value that is off by some decimal points, say, 4567.34 instead of 45.6734, could bias our calculating and bias the result.  We do not have the time to check for incorrect values amongst all the data.  We need a method that is resistant to extremes and incorrect data.  With median, we can use the middle value in our sorted list. 

To make sure this was a sensible idea I pulled up DB Browser to look at all stations of the same name which have similar coordinates.

```SQL
SELECT DISTINCT(name_coords), station_name, COUNT(*) AS cnt
FROM 
	(SELECT (start_station_name || " " || round(start_lat,4) || " " || round(start_lng,4)) AS name_coords, start_station_name as station_name
	FROM tripdata
	UNION ALL
	SELECT (end_station_name || " " || round(end_lat,4) || " " || round(end_lng,4)) AS name_coords, end_station_name as station_name
	FROM tripdata) A
GROUP BY station_name
ORDER BY station_name, cnt DESC
```

In essence, this query gets a count of rows that have the same station name and coordinate precision of 4 decimal points.  We get something like this.

![stations_similar_coordinates](/dylanjackson/images/bike-share/stations_similar_coordinates.PNG)

The result was the same across all stations.  For each station, the bulk of the coordinates are less than 0.0001 from each other.  4 decimal point precision gives us an accuracy of roughly 11 meters and with these results, I believe it's safe to trust the median value for our uniform coordinates.

Back in RStudio we can obtain the median value for each station with the following.

```R
station_data_medians <-station_data %>% 
  group_by(proper_name) %>% 
  summarize(median_lat = median(lat), median_lng = median(lng))
```

Just for fun, let's pop this data into Tableau and see how it looks.

![station_map](/dylanjackson/images/bike-share/station_map.PNG)
