---
title: "Bike Share: Final Thoughts"
date: 2021-08-06T10:22:18-04:00
draft: false
---

For the final of my Google Data Analytics course, I was tasked using the last 12 months of bike-shares ride data to produce insights into the key differences between casual riders and members.  The complete Tableau visualization can be found [here](https://public.tableau.com/app/profile/dylan.jl/viz/Bike-ShareCaseStudy/Story1).

# Final Thoughts

I'm satisfied with what I was able to find.  From what I was able to gather there was certainly enough insight to take meaningful action towards their intended goals.  But, as I worked through the data I kept thinking, "I want more!"  username IDs, and user addresses would have allowed a much more in-depth analysis.  With Username Id's we could have gotten a sense of not just the casual group as a whole but the average user.  Being constrained to just the ride information felt like being in a walled garden whose fence I could see over.  I knew what I could do with what I had, and I knew what I could if I had more.

This data could also be a great opportunity for machine learning.  Many rows of data were missing information such as the stations they started and ended in.  But with the data around that, we could find with reasonable confidence what this missing data is.  I believe training on the date information would be particularly helpful.  We already know that different time granularities show clear trends concerning their starting and ending stations.  We could use the date and duration info of the rows which were missing their station information and potentially draw a meaningful conclusion about where that ride started or ended.  This is something I would love to explore more in the future.

## Conclusions

This was a great opportunity to learn about myself.  At every stage of the process, there was a multitude of ways to approach the situation and arrive at the same destination.  I became aware of what processes and tools were comfortable, which enabled me to see where I could improve.  Concerning data cleaning and integrity, SQL, R, and Excel could have been used exclusively to perform the tasks.  Yet I found myself using a mixture of all three.

Now of course this is not an issue in itself.  Naturally, different tools are better suited for different jobs.  For example, we all learn the row cap within excel make certain tasks only viable with other tools.  Regardless, I look forward to becoming more versatile and expanding my knowledge with the tools I have.
