---
title: "Tableau Public"
date: 2021-06-10T10:30:22-04:00
draft: false

---

**This information is intended to be used with Tableau Public.**

# Connecting Tableau to a CSV File

- On the Left Hand side under connect, select Text File

# Navigating Tableau

## Data Tab

Shows the different columns of data from the associated Data Source

- ### Dimensions - Independent Variables

  - Categorical Values

- ### Measures - Dependent Variables

  - Numerical Values

- ### Example)

  - Dimensions contains a variable Regions
  - Measures contains a variable Units
  - Using Regions as the Independent Variable We can find out how many Units are sold per Region

## Calculated Field (New Measure)

### Creating a Calculated Field

<video controls src="/dylanjackson/videos/create_calculated_field.mp4"></video>

### Creating a Calculated Field with Blended Data

- Values are aggregated due to the nature of blending

<video controls src="/dylanjackson/videos/create_blended_calc.mp4"></video>

# Visualization:

### Changing Colors:

<video controls src="/dylanjackson/videos/change_color.mp4"></video>

# Adding Labels and Formatting:

### Creating a Label:

<video controls src="/dylanjackson/videos/add_label.mp4"></video>

### Editing a Label:

<video controls src="/dylanjackson/videos/change_label.mp4"></video>

### Change Label to Table Calculation

<video controls src="/dylanjackson/videos/label_to_percent.mp4"></video>

# Formatting

### Getting to format settings

<video controls src="/dylanjackson/videos/format.mp4"></video>

# Time Series

## Granularity

### Change Granularity

<video controls src="/dylanjackson/videos/granularity.mp4"></video>

# Highlighters

- Highlighters are used to make data that fits a criteria stand out visually.
- Highlighting keeps values which do not fit the criteria, but does not accentuate them.
  - Vs Filters which only displays results which fit the criteria.

# Filters

- Filters Remove data from analysis before visualizing, unlike highlighters.

### Create Filter and Filter Shortcut

<video controls src="/dylanjackson/videos/filter.mp4"></video>

### Single Value Filter

<video controls src="/dylanjackson/videos/single_value_filter.mp4"></video>

### Apply a Filter Across Multiple Worksheets

- This will apply the filter to new worksheets as well.

<video controls src="/dylanjackson/videos/apply_filter_all_worksheets.mp4"></video>

# Bins

- Bins Categorize a field into groups.  For instance grouping ages in groups of 5.

<video controls src="/dylanjackson/videos/create_bin.mp4"></video>

# Parameters

A parameter is a workbook variable such as a number, date, or string that can replace a constant value in a calculation, filter, or reference line.

### Creating and Attaching a Parameter

<video controls src="/dylanjackson/videos/create_parameter.mp4"></video>

# Hierarchy

Hierarchies are used to group similar fields with different granularities

ex: County,State,Country

### Creating a Hierarchy

<video controls src="/dylanjackson/videos/create_hierarchy.mp4"></video>

### Changing Granularity In a Hierarchy

<video controls src="/dylanjackson/videos/changing_hierarchy_granularity.mp4"></video>

# Grouping

- Creates a new Dimension from selected values of a field

<video controls src="/dylanjackson/videos/create_group.mp4"></video>

<video controls src="/dylanjackson/videos/create_group2.mp4"></video>

# Dashboards

### Creating a Dashboard

<video controls src="/dylanjackson/videos/create_dashboard.mp4"></video>

### Adding a Filter to the Dashboard

- Filter applied on the dashboard affect any Worksheets which also utilize the same data

<video controls src="/dylanjackson/videos/dashboard_add_filter.mp4"></video>

### Adding an Interactive Action to the Dashboard

- In this Video the interactive Action filters the data on the Customer Scatter Plot based on what Country we are selecting.  We can either select Country by click or by hover.

<video controls src="/dylanjackson/videos/dashboard_create_action.mp4"></video>

# Stories

Stories are where you aggregate your charts and discoveries.  Stories allow multiple dashboards and worksheets without applied filters being shared between them.  This means a page in a story can use a set of filters on a sheet/dashboard to display insights, while another page with the same sheet/dashboard can have different filters.

### Quick Visual Overview

<video controls src="/dylanjackson/videos/story_overview.mp4"></video>

# Joining Data

### Joining in Tableau

<video controls src="/dylanjackson/videos/join_tables.mp4"></video>

- Joining Tables finds a common field between two sets of data to form the relationship between the tables
- In this example it is join the tables through their Order Id

## Kinds of Joins

- Assuming we have two different tables (A and B) both with a field named "Employees".  We will use this as our relationship between the tables.

### Inner Join

- Returns data from both tables only if the employee is present in both table A and table B.

![](https://coderhalt.com/wp-content/uploads/2021/04/innerjoin.png)

### Left Join

- Returns all data from Table A as well as data from Table B where there was a match in Employees

![](https://coderhalt.com/wp-content/uploads/2021/04/lrft-join.png)

### Right Join

- Returns all data from Table B as well as data from Table A where there was a match in Employees

![](https://coderhalt.com/wp-content/uploads/2021/04/right-join.png)

### Full Outer Join

- Returns all values from both Tables A and B

![](https://coderhalt.com/wp-content/uploads/2021/04/full-join.png)

## Joins With Duplicate Values

- If the fields we use to form the relationship between tables contain multiple values (Table B) we see that the data from the related table (Table A) will be duplicated as well.

### Table A

| Order # | Region | Status |
| ------- | ------ | ------ |
| 001     | North  | Unpaid |
| 002     | North  | Unpaid |
| 003     | North  | Paid   |
| 004     | North  | Paid   |

### Table B

| Order # | Item    | Sales |
| ------- | ------- | ----- |
| 001     | Chair   | 97    |
| 001     | Desk    | 123   |
| 002     | Stapler | 8     |
| 003     | Pen     | 3     |
| 003     | Pencil  | 1     |
| 003     | Eraser  | 1     |

### Resulting Inner Join

| Order # | Region | Status | Order # | Item    | Sales |
| ------- | ------ | ------ | ------- | ------- | ----- |
| 001     | North  | Unpaid | 001     | Chair   | 97    |
| 001     | North  | Unpaid | 001     | Desk    | 123   |
| 002     | North  | Unpaid | 002     | Stapler | 8     |
| 003     | North  | Paid   | 003     | Pen     | 3     |
| 003     | North  | Paid   | 003     | Pencil  | 1     |
| 003     | North  | Paid   | 003     | Eraser  | 1     |

## Joining on Multiple Fields

- This is when we specify multiple fields to be used to form the relationship between tables

# Blending Data

- Blending data is a way to combine multiple data sources.  Unlike a Join, they are never truly combined.  Tableau queries each data source separately and aggregates the results.  The result is always a left join.

- Blending is on a per worksheet basis, meaning, custom blends will not carry over to other worksheets.

- Common fields with the same name are automatically blended.

- Blending occurs at the level of granularity of your worksheet and any aggregation needed to get to that granularity is done before blending.

- You can tell a field has been blended if you see a chain link next to the name.

  ![blend_link](/dylanjackson/images/blend_link.jpg)

  ## Manually Blending Data

  1. Go to Data > Edit Blend Relationships
  2. Select Custom and Click Add.

<video controls src="/dylanjackson/videos/blending.mp4"></video>

# When to Use Join or Blend

- Join when combining data at the row level
- Blend when:
  - Data sources have different levels of granularity
  - Data sources come from different systems (csv vs excel)

# Relationships

Relationships form a data model with both a logical and physical layer

[Documentation on Relationships vs Join](https://help.tableau.com/v2021.1/public/desktop/en-us/datasource_relationships_learnmorepage.htm)

- Logically we create relationships
- Physically we go into the physical layer of a table and create a join



<video controls src="/dylanjackson/videos/create_relationship.mp4"></video>

# Data Cleaning

### Using Tableau's Data Interpreter

<video controls src="/dylanjackson/videos/data_interpreter.mp4"></video>

### Splitting a Column into Multiple Columns

<video controls src="/dylanjackson/videos/using_spit.mp4"></video>

### Viewing Metadata

<video controls src="/dylanjackson/videos/viewing_metadata.mp4"></video>

### Filtering on a Data Source

<video controls src="/dylanjackson/videos/filters_on_datasource.mp4"></video>

## Pivots

Pivots essentially turns multiple fields into two fields.  One field is the values that were in the original fields.  The other field's values are what the name of the field was before being collapsed.

### Applying a Pivot

<video controls src="/dylanjackson/videos/create_pivot_table.mp4"></video>

# Clustering

For certain sets of data, clusters can be used to identify different groups which share the same characteristics.

### Creating a Cluster

<video controls src="/dylanjackson/videos/create_cluster.mp4"></video>

### Using a Cluster as a Dimension

<video controls src="/dylanjackson/videos/custer_as_dimension.mp4"></video>

# Dual Axis Chart

Dual Axis Charts display multiple queries on the same chart using a shared axis.

- Make sure to Synchronize your axis when needed 

<video controls src="/dylanjackson/videos/create_dual_axis.mp4"></video>

# Map

## Map Types

### Polygon Map

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdnl.tblsft.com%2Fsites%2Fdefault%2Ffiles%2Fbranded-header%2Fpolygon_maps_0.png&f=1&nofb=1)

### Point Map

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fevolytics.com%2Fwp-content%2Fuploads%2F2016%2F03%2FTableau-Symbol-Map-by-Postal-Code.jpg&f=1&nofb=1)

### Line Map

![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpublic.tableau.com%2Fs%2Fsites%2Fdefault%2Ffiles%2Fmedia%2Fthumbnail-image2_2.png&f=1&nofb=1)

### Fix: Tableau does recognize a field as geographically related:

<video controls src="/dylanjackson/videos/fix_map.mp4"></video>

# Animations

Animation allow you to visualize data changes over a period of data, such as time.

<video controls src="/dylanjackson/videos/create_animation.mp4"></video>

# Spatial Files

Spatial Files contain Geospatial Data, or data related to or containing information about a specific location on the Earth's surface.

## Supported Spatial Files

- Shapefiles, MapInfo tables, KML and GeoJSON

## How Spatial Files Work in Tableau

- Tableau transforms the files using the longitude and latitude into geometry fields

# Visualizations in Tooltips

<video controls src="/dylanjackson/videos/insert_tooltip.mp4"></video>

