---
title: "SQL with PostgreSQL"
date: 2021-06-04T10:30:22-04:00
draft: false
---

This information is intended to be used with PostgreSQL

# General Information

## Where to Execute Code

- Select your database
- Go to Tool -> Query Tool

## Executing Select Code

- Highlight the code you want to run and then hit run (F5)

## Comments `--`

```sql
-- This is a comment, it will be ignored when executing code.
```



## Create Table and Insert Statements

### Create a Table

```sql
-- Creates a table called departments with two columns
-- department column, and division,
-- both a series of chars with a max length of 100
-- The primary key is set to department.
create table departments (
    department varchar(100),
    division varchar(100),
    primary key (department)
    );
```

- primary key is unique

### Inserting Data into Table

```sql
-- This creates a new entry into the departments table
-- where department = 'Automotive' and division = 'Auto & Hardware'
insert into departments values ('Automotive','Auto & Hardware')
```

## Prepare the Database

## Grabbing a Table

```SQL
-- Select everything from the Employees table
SELECT * FROM EMPLOYEES
```

## Renaming

- A Column:

  ```sql
  -- Grab the first name column, but change it's name
  -- This change is not reflected in the original table
  SELECT FIRST_NAME AS The_New_Name
  SELECT FIRST_NAME AS "The New Name"
  ```

# Alias

- An Alias allows us to reference data without explicitly typing out the entire data sources name.

```sql
-- Gives Employees and Departments tables an alias of E and D
SELECT E.DEPARTMENT
FROM EMPLOYEES E, DEPARTMENTS D
```

# Clause

## SELECT

- Tells the database to retrieve information

- ```SQL
  -- Retrieve all information from a table
  SELECT *
  ```

## FROM

- Specifies where to retrieve the data from

- ```sql
  -- Retrieve all data from the employees table
  SELECT *
  FROM EMPLOYEES
  ```

  

## WHERE

- Used for Filtering, Get results where the following statement is true

- ```sql
  -- Returns only results where their name is Tim
  WHERE NAME = 'Tim'
  ```

  

## AND

- Used in-between statements, AND is used chain statements together

- Both statements must be True in order to retrieve the associated data

- ```sql
  -- Returns only results where their name is Tim and their age is 20
  WHERE NAME = 'Tim' AND AGE = 20
  ```

## OR

- Similar to AND, OR is used to chain statements together

- Only 1 statement must be true in order to retrieve the associated data

- ```sql
  -- Returns results where their name is Tim or their age is 20
  WHERE NAME = 'Tim' OR AGE = 20
  ```

## ALL

- Used for comparing a single value with multiple values
- returns true when the statement is true for all subqueries

```sql
-- Returns Results in which age is greater than all ages from another_table
WHERE AGE > ALL(SELECT AGE FROM ANOTHER_TABLE)
```

## ANY

- Used for comparing a single value with multiple values
- returns true when the statement is true for at least one subquery

```SQL
-- Returns results in which age is greater than 
-- at least one instance of age from another_table
WHERE AGE > ANY(SELECT AGE FROM ANOTHER_TABLE)
```

## CASE

- Creating a new column populated by the results of conditional statements

```SQL
-- In this example we are creating a new column called how_well_paid
-- Each value in the how_well_paid column is determined by
-- The results of the comparisons we make in the CASE clause
SELECT FIRST_NAME, SALARY, 
	CASE WHEN SALARY < 100000 THEN 'UNDER PAID'
		WHEN SALARY > 100000 AND
			SALARY < 165000 THEN 'PAID WELL'
		ELSE 'PAID REAL WELL'
	END as how_well_paid
FROM EMPLOYEES
ORDER BY SALARY DESC

-- Custom column names with count of occurence using case
SELECT 	SUM(CASE WHEN SALARY < 100000 THEN 1 ELSE 0 END)
		AS UNDER_PAID,
		SUM(CASE WHEN SALARY > 100000 AND
		SALARY < 160000 THEN 1 ELSE 0 END) 
		AS PAID_WELL,
		SUM(CASE WHEN SALARY > 160000 THEN 1 ELSE 0 END)
		AS EXECUTIVE
FROM EMPLOYEES

```



## Chaining Logic `( )`

Much like math, we use PEMDAS to order our operations

- `( )` is used to couple our logic

```sql
-- Restuls are people who:
-- Work in the Clothing department and make over 100k
-- Workin the furniture department and make less than 50k
WHERE (DEPARTMENT = 'Clothing'
	AND SALARY > 100000)
OR (DEPARTMENT = 'Furniture'
	AND SALARY < 50000)
```

## ORDER BY

- Used to order the data being displayed

```SQL
-- Resulting data will be sorted by the employee_id
SELECT *
FROM EMPLOYEES
ORDER BY EMPLOYEE_ID
```

### DESC

- Reverse how its ordered

```SQL
ORDER BY DEPARTMENT DESC
```

### ASC

- Straight forward ordering

```SQL
ORDER BY DEPARTMENT ASC
```

## DISTINCT

- Returns only the unique values

```sql
-- Resulting departments
SELECT DISTINCT DEPARTMENT
FROM EMPLOYEES
```

## LIMIT

- Limits the number of results to show

```sql
-- Shows only the first 10 results
SELECT *
FROM EMPLOYEES
LIMIT 10
```

## FETCH FIRST

Shows first X results

```Sql
-- Shows only the first 10 results
SELECT *
FROM EMPLOYEES
FETCH FIRST 10 ROWS ONLY
```

## DELETE

- Used to remove rows from the table

```sql
-- deletes rows where ID is not 12
DELETE
FROM DUPES
WHERE ID != 12
```

# Window Functions

Window Functions allow us to perform calculations across a set of table rows.  Unlike aggregate functions, window functions do not group rows into a single output row.

## OVER

- Great alternative to subqueries, less expensive
- Uses Window Frames to prevent recursive querying

```SQL
-- Returns the first_name of every employee
-- As well as their department, and number of
-- employees working in said department
SELECT FIRST_NAME, DEPARTMENT, 
	COUNT(*) OVER(PARTITION BY DEPARTMENT)
FROM EMPLOYEES
-- Returns first_name, department, and count of employees working in the department, in which their region_id is 4
SELECT first_name, department, COUNT(*) OVER(PARTITION BY department)
FROM employees
WHERE region_id = 4
```

### OVER AND ORDER BY

```sql
-- Returns first_name, hire_date, salary,
-- and cumulative salary, ordered by hire_date
SELECT first_name, hire_date, salary,
	SUM(salary) OVER(ORDER BY hire_date RANGE BETWEEN 
    UNBOUNDED PRECEDING AND CURRENT ROW) as running_total_salary
FROM employees
-- Last column returns sum of salary from current row and the row before
SELECT first_name, hire_date, department, salary,
	SUM(SALARY) OVER(ORDER BY hire_date ROWS BETWEEN 1 PRECEDING AND CURRENT ROW)
FROM employees


```

## Rank

```sql
-- Creates a int column, ranking employees by department based on their salary.
-- Highest = 1 lowest = num employees in department
SELECT first_name, email, department, salary,
RANK() OVER(PARTITION BY department ORDER BY salary DESC)
FROM employees
-- Additional Filtering
-- WHERE does not work as you'd think
-- Needs to be subqueried
SELECT first_name, department, salary
FROM
	(SELECT first_name, email, department, salary,
	RANK() OVER(PARTITION BY department ORDER BY salary DESC)
FROM employees) a
WHERE rank = 1

```

## NTILE ( )

- Similar to Rank but for groups of ranks, like a bracket
- Evenly distributes the resulting rows into N different brackets.
  - Which bracket they end up in is determined by how the table is sorted

```SQL
-- puts employees into brackets based on salary
SELECT first_name, email, department, salary,
	NTILE(5) OVER(PARTITION BY department ORDER BY salary DESC)
FROM employees
```

## FIRST_VALUE ( )

- Takes the first value which occurs in each group

```sql
-- Last column will display the first salary
-- That occurs for each department
SELECT first_name, email, department, salary,
	FIRST_VALUE(SALARY) OVER(PARTITION BY department ORDER BY salary DESC)
FROM employees
```

## NTH_VALUE( )

-- Grabs the nth_value which occurs in each group

```SQL
SELECT first_name, email, department, salary,
	NTH_VALUE(SALARY,5) OVER(PARTITION BY department ORDER BY salary DESC)
FROM employees
```

## LEAD( ) and LAG( )

- Lead - returns selected value from next row
- Lag - returns selected value from previous row

```sql
-- last column will have the salary of
-- The column that occurs after current row
SELECT first_name, last_name, salary,
	LEAD(salary) OVER() next_salary
FROM employees
-- last column will have the salary of
-- The column that occurs before current row
SELECT first_name, last_name, salary,
	LAG(salary) OVER() next_salary
FROM employees
```



# Data Formatting

## UPPER( )

- Results in cell are all uppercase

```SQL
-- Result is the employees first_names in all Uppercase
SELECT UPPER(FIRST_NAME)
FROM EMPLOYEES
```

## LENGTH( )

- Results are the length for each cell

```SQL
-- Result is the number of chars in each employees first_name
SELECT LENGTH(FIRST_NAME)
FROM EMPLOYEES
```

## TRIM( )

- Removes whitespace

```SQL
-- Result is 'Hello There' notice no whitespace preceding or following
SELECT TRIM('    HELLO THERE    ')
```

## Concat ||

- Concatenates results of first and second argument into single cell

```sql
-- Result is a column containing the first_name and last_name
-- Separtated by a ' '
SELECT FIRST_NAME || ' ' || LAST_NAME AS "Name"FROM EMPLOYEES
```

## Boolean Expression

- Creates column with the evaluation of the statement, True or False

```SQL
-- Returns bool column If their salary is above 120000
SELECT (SALARY > 120000)
-- Returns bool column if first_name or department contains 'clothing'
SELECT DEPARTMENT, ('Clothing' IN (DEPARTMENT, FIRST_NAME))
-- Return bool column if department contains 'oth' in name
SELECT DEPARTMENT, (DEPARTMENT LIKE '%oth%')
```

# GROUP BY

- Groups rows that have the same values into summary rows.

```SQL
-- Creates column for each unique row
-- Telling you the count of each value
SELECT MAKE ,COUNT(*)
FROM CARS
GROUP BY MAKE
```

## HAVING

- Similar to WHERE, HAVING is used to filter data that has been grouped

```sql
-- Only show department with more than 25 occurences
SELECT DEPARTMENT
FROM EMPLOYEES
GROUP BY DEPARTMENT
HAVING COUNT(*) > 25
```

## GROUPING SETS

```SQL
-- Grouping Sets allow grouping by multiple columns
-- While still distinguishing the results unique to each column
-- In this example we can view the sum of units sold individually
-- For each continent, country, and city.
SELECT CONTINENT, COUNTRY, CITY, SUM(UNITS_SOLD)
FROM SALES
GROUP BY GROUPING SETS(CONTINENT, COUNTRY, CITY)
```

## ROLLUP

```SQL
-- Rollup is similar to Grouping Sets but displays
-- The relationshis between the columns
-- While still maintaining the results individual columns.
SELECT CONTINENT, COUNTRY, CITY, SUM(UNITS_SOLD)
FROM SALES
GROUP BY Rollup(CONTINENT, COUNTRY, CITY)
```

## CUBE

```SQL
-- Similar to Grouping Sets, the difference being
-- Cube will display the results of every possible
-- Permutation of the data.
SELECT CONTINENT, COUNTRY, CITY, SUM(UNITS_SOLD)
FROM SALES
GROUP BY Cube(CONTINENT, COUNTRY, CITY)
```



# Aggregate Functions

Performs a calculation on a column and returns a Single Value

## MIN( )

```sql
-- Returns smallest value in column
SELECT MIN(SALARY)
FROM EMPLOYEES
```

## MAX( )

```sql
-- Returns cell with highest value
SELECT MAX(SALARY)
FROM EMPLOYEES
```

## AVG( )

```sql
-- Returns the average of all values in the cell
SELECT AVG(SALARY)
FROM EMPLOYEES
```

## SUM( )

```sql
-- Returns cell with sum of all column values
SELECT SUM(SALARY)
FROM EMPLOYEES
```

## COUNT( )

```sql
-- Returns cell with the count of non NULL rows in column
SELECT COUNT(SALARY)
FROM EMPLOYEES
```

# JOIN

- Using a common denominator in both tables join them to combine the information from both

```SQL
-- Simple Examples
-- Get first name and country for each employee
SELECT FIRST_NAME, COUNTRY
FROM EMPLOYEES, REGIONS
WHERE EMPLOYEES.REGION_ID = REGIONS.REGION_ID
-- Get Using aliases and joining to get data from multiple tables
SELECT FIRST_NAME, EMAIL,d.DEPARTMENT, DIVISION, COUNTRY
FROM DEPARTMENTS d, EMPLOYEES e, REGIONS r
WHERE e.DEPARTMENT = d.DEPARTMENT
	AND e.REGION_ID = r.REGION_ID
	AND EMAIL IS NOT NULL	
-- Get total count of employees for each country
SELECT COUNTRY, COUNT(E.*)
FROM EMPLOYEES E, REGIONS R
WHERE E.REGION_ID = R.REGION_ID
GROUP BY COUNTRY
```

## INNER

```SQL
-- inner joining 3 tabales
SELECT FIRST_NAME, EMAIL, DIVISION, COUNTRY
FROM EMPLOYEES INNER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.DEPARTMENT 
	INNER JOIN REGIONS ON EMPLOYEES.REGION_ID = REGIONS.REGION_ID
WHERE EMAIL IS NOT NULL
```

## LEFT

```SQL
-- return all distinct departments on the employees table
-- regardless of if they appear on the departments table
SELECT DISTINCT EMPLOYEES.DEPARTMENT, DEPARTMENTS.DEPARTMENT
FROM EMPLOYEES LEFT JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.DEPARTMENT;

```



## RIGHT

```SQL
-- return all distinct departments on the departments table
-- regardless of if they appear on the employees table
SELECT DISTINCT EMPLOYEES.DEPARTMENT, DEPARTMENTS.DEPARTMENT
FROM EMPLOYEES RIGHT JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.DEPARTMENT;
```



## OUTER

```SQL
-- Returns all departments in the employee table
-- Which are also present in the departments department column
SELECT DISTINCT EMPLOYEES.DEPARTMENT
FROM EMPLOYEES FULL OUTER JOIN DEPARTMENTS ON EMPLOYEES.DEPARTMENT = DEPARTMENTS.DEPARTMENT
```

## CROSS JOIN

- Returns cartesian product of the two tables
- Can't wait to find a reason to use this.

```sql
-- Returns every permutation of combining employees and departments tables
SELECT *
FROM EMPLOYEES CROSS JOIN DEPARTMENTS
```



# UNION

## UNION

- combines the result-set of two or more `SELECT` statements.
- Union selects distinct values by default, if you want duplicates use UNION ALL

```sql
-- Result is distinct values from both tables
SELECT DEPARTMENT
FROM EMPLOYEES
UNION
SELECT DEPARTMENT
FROM DEPARTMENTS
```

## UNION ALL

- combines the results of two or more `Select` statements, allowing duplicates

```sql
-- Returns all distinct departments in employees
-- and all departments in departments as one column
SELECT DISTINCT DEPARTMENT
FROM EMPLOYEES
UNION ALL
SELECT DEPARTMENT
FROM DEPARTMENTS

-- returns each department and number of employees
-- last cell is the total count of all employees
SELECT DEPARTMENT, COUNT(*)
FROM EMPLOYEES
GROUP BY DEPARTMENT
UNION ALL
SELECT 'TOTAL', COUNT(*)
FROM EMPLOYEES
ORDER BY COUNT
```

## EXCEPT

- Removes all results from the first query which are also found in the second
- Useful when filtering out data which is unique to only certain tables

```sql
-- Returns departments from employees which are not in the departments department column
SELECT DISTINCT DEPARTMENT
FROM EMPLOYEES
EXCEPT
SELECT DEPARTMENT
FROM DEPARTMENTS
```



# Comparison Operators:

## `=` If A equals B

- `=` is a comparison  A is checked if it is equal to B with a Boolean result.
- This does not set A equal to B.

```sql
-- Returns only rows in which the name = 'Tom' is True
WHERE NAME = 'Tom'
```



## `like` if A is similar to B

- Useful for analyzing substring, `like` looks for the presence of a specified pattern

- Specified pattern must be wrapped in % %

```sql
-- Returns results where STUDENT_NAME
-- contain 'ch' in it
WHERE STUDENT_NAME LIKE '%ch%'
-- Returns results where STUDENT_NAME
-- contains 'nd' at the end
WHERE STUDENT_NAME LIKE '%nd'


```



## `< > <= >=`

- Returns a Boolean on the relationship between A and B



## Filtering Operations ( IN, NOT IN, IS NULL, BETWEEN):

- ```sql
  IN, NOT IN, IS NULL, BETWEEN
  ```

  ### NOT, !=, <>
  
  - Turns true to false and vise verse
  
  - ```SQL
    -- Returns departments which are not named 'Clothing'
    WHERE NOT DEPARTMENT = 'Clothing'
    WHERE DEPARTMENTS != 'Clothing'
    WHERE DEPARTMENTS <> 'Clothing'
    ```
  
  ### NULL
  
  - Empty Data
  
  - Can't be used for comparison operators, since it is nothing.
  
  - ```SQL
    WHERE EMAIL IS NULL
    WHERE NOT EMAIL IS NULL
    ```
  
  ### IS & ISNOT
  
  - Used when checking for the presence of a value in a Series of values
  
  ```sql
  -- Returns departments which are 'Sports','First Aid' or 'Toys'
  WHERE DEPARTMENT IN ('Sports','First Aid','Toys')
  
  ```
  
  - Also used to check for null
  
  ```SQL
  -- Returns all results in which department is null
  WHERE DEPARTMENT IS NULL
  ```
  
  
  
  ### BETWEEN
  
  - Used to check if a value falls between a range of parameters
  
  - ```SQL
    -- returns True for salaries between 10000 and 30000
    WHERE SALARY BETWEEN 10000 AND 30000
    ```

# String Functions:

## SUBSTRING( )

- Used for slicing a string

```sql
-- Returns everything from the 9th character to the end
-- 'a string'
SELECT SUBSTRING('This is a string' FROM 9)
-- Returns everything from the 1st character up to and including the 4th
-- 'This'
SELECT SUBSTRING('This is a string' FROM 1 FOR 4 )
```

## REPLACE( )

```SQL
-- Returns all departments, Clothing is now called Burlap
SELECT REPLACE(DEPARTMENT, 'Clothing','Burlap')
FROM DEPARTMENTS
```

## POSITION( )

```sql
-- Returns position char is at
SELECT POSITION('@' IN EMAIL) 'AT_SYMBOL'
FROM EMPLOYEES
```

## COALESCE( )

```sql
-- Instead of NULL, show the string 'NO EMAIL'
SELECT COALESCE(EMAIL,'NO EMAIL') AS EMAIL
FROM EMPLOYEES
```

# Subqueries

Nested select statements

```SQL
-- In this example (SELECT DEPARTMENT FROM DEPARTMENTS) is the subquery
-- We are obtaining all department values from departments.
-- Then checking if the employee department is not in that Series

-- Returns all rows in employees table in which their department
-- is not in the departments column of the departments table
SELECT *
FROM EMPLOYEES
WHERE DEPARTMENT NOT IN (SELECT DEPARTMENT FROM DEPARTMENTS)
-- Returns the salary of employees who make over 150000
SELECT THE_ALIAS.YEARLY_SALARY
FROM (SELECT FIRST_NAME EMPLOYEE_NAME, SALARY YEARLY_SALARY
	 FROM EMPLOYEES WHERE SALARY > 150000) THE_ALIAS
```

## Correlated Subqueries

- subquery run for every row of main query.
- Uses information from outer query

```SQL
-- For each row in employees we are getting the average salary
-- of everyone in the same region and checking if their salary
-- is larger.
-- Doing this requires knowledge of the primary query to be present
-- In the subquery.
-- As such the subquery runs for every row.

-- Returns rows in which the salary of the individual is higher
-- than the average salary of the region they are in
SELECT FIRST_NAME, SALARY
FROM EMPLOYEES e1
WHERE SALARY > (SELECT ROUND(AVG(SALARY))
				FROM EMPLOYEES e2
			   WHERE e1.REGION_ID = e2.REGION_ID)
```

# VIEW

- Used to save aggregated data into a non modifiable table.

```sql
-- We Create a view joining employees, departments, and regions
CREATE VIEW V_EMPLOYEE_INFORMATION AS
SELECT FIRST_NAME, EMAIL, E.DEPARTMENT, SALARY, DIVISION
FROM EMPLOYEES E, DEPARTMENTS D, REGIONS R
WHERE E.DEPARTMENT = D.DEPARTMENT
AND E.REGION_ID = R.REGION_ID

-- And now we can query the view as though it is a table
SELECT FIRST_NAME 
FROM V_EMPLOYEE_INFORMATION
```

## Inline VIEW

- By creating a subquery, the primary query is no longer referencing the original table.

```sql
SELECT *
FROM (SELECT * FROM DEPARTMENTS)
```
